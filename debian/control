Source: ncat-w32
Section: utils
Priority: optional
Maintainer: Kali Developers <devel@kali.org>
Uploaders: Devon Kearns <dookie@kali.org>,
           Ben Wilson <g0tmi1k@kali.org>,
Build-Depends: debhelper-compat (= 12)
Standards-Version: 3.9.3
Homepage: https://nmap.org/ncat/
Vcs-Git: https://gitlab.com/kalilinux/packages/ncat-w32.git
Vcs-Browser: https://gitlab.com/kalilinux/packages/ncat-w32

Package: ncat-w32
Architecture: all
Depends: ${misc:Depends},
         kali-defaults,
Description: Netcat for the 21st century
 Ncat is a feature-packed networking utility which reads and writes data across
 networks from the command line. Ncat was written for the Nmap Project as a
 much-improved reimplementation of the venerable Netcat. It uses both TCP and
 UDP for communication and is designed to be a reliable back-end tool to
 instantly provide network connectivity to other applications and users. Ncat
 will not only work with IPv4 and IPv6 but provides the user with a virtually
 limitless number of potential uses.
 .
 Among Ncat’s vast number of features there is the ability to chain Ncats
 together, redirect both TCP and UDP ports to other sites, SSL support, and
 proxy connections via SOCKS4 or HTTP (CONNECT method) proxies (with optional
 proxy authentication as well). Some general principles apply to most
 applications and thus give you the capability of instantly adding networking
 support to software that would normally never support it.
